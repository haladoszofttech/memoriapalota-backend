package hu.memo.backend.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.memo.backend.response.MemoFindMediaResponse;
import hu.memo.backend.response.MemoGetMediaResponse;
import org.h2.util.StringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class MemoBackendIntegrationTest {

    @Autowired
    private MockMvc mvc;

    ObjectMapper mapper = new ObjectMapper();

    @Test
    public void findAllImageReturnsAllImages() throws Exception {
        int expectedResponseSize = 2;

        // create file
        String expectedOriginalFileName = "hello.png";
        MockMultipartFile file = new MockMultipartFile("file", expectedOriginalFileName, MediaType.MULTIPART_FORM_DATA_VALUE, "Hello World!".getBytes());
        String imageId = sendCreateMediaRequest("/memo/image", file);

        // create file
        String expectedOtherOriginalFileName = "bye.jpeg";
        MockMultipartFile otherFile = new MockMultipartFile("file", expectedOtherOriginalFileName, MediaType.MULTIPART_FORM_DATA_VALUE, "Bye World!".getBytes());
        String otherImageId = sendCreateMediaRequest("/memo/image", otherFile);

        String images = mvc.perform(get("/memo/image"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MemoFindMediaResponse> mediaResponses = Arrays.asList(mapper.readValue(images, MemoFindMediaResponse[].class));

        assertEquals(expectedResponseSize, mediaResponses.size());

        assertEquals(imageId, String.valueOf(mediaResponses.get(0).id));
        assertEquals(expectedOriginalFileName, mediaResponses.get(0).name);

        assertEquals(otherImageId, String.valueOf(mediaResponses.get(1).id));
        assertEquals(expectedOtherOriginalFileName, mediaResponses.get(1).name);

        sendDeleteMediaRequest("/memo/image/", imageId);
        sendDeleteMediaRequest("/memo/image/", otherImageId);
    }

    @Test
    public void create_read_delete_image() throws Exception {
        // create
        byte[] expectedContent = "Hello World!".getBytes();
        String expectedOriginalFileName = "hello.png";
        MockMultipartFile file = new MockMultipartFile("file", expectedOriginalFileName, MediaType.MULTIPART_FORM_DATA_VALUE, expectedContent);
        String imageId = sendCreateMediaRequest("/memo/image", file);

        assertNotNull(imageId);
        assertTrue(StringUtils.isNumber(imageId));

        // read
        String fileContent = sendReadMediaRequest("/memo/image/", imageId, status().isOk());

        ObjectMapper mapper = new ObjectMapper();
        MemoGetMediaResponse memoGetMediaResponse = mapper.readValue(fileContent, MemoGetMediaResponse.class);

        assertEquals(imageId, String.valueOf(memoGetMediaResponse.id));
        assertEquals(expectedOriginalFileName, memoGetMediaResponse.fileName);
        assertArrayEquals(expectedContent, memoGetMediaResponse.content);

        // delete
        sendDeleteMediaRequest("/memo/image/", imageId);

        // check doesn't exist
        sendReadMediaRequest("/memo/image/", imageId, status().isNotFound());
    }

    @Test
    public void create_read_delete_video() throws Exception {
        // create
        byte[] expectedContent = "Hello World!".getBytes();
        String expectedOriginalFileName = "hello.mp4";
        MockMultipartFile file = new MockMultipartFile("file", expectedOriginalFileName, MediaType.MULTIPART_FORM_DATA_VALUE, expectedContent);
        String videoId = sendCreateMediaRequest("/memo/video", file);

        assertNotNull(videoId);
        assertTrue(StringUtils.isNumber(videoId));

        // read
        String fileContent = sendReadMediaRequest("/memo/video/", videoId, status().isOk());

        MemoGetMediaResponse memoGetMediaResponse = mapper.readValue(fileContent, MemoGetMediaResponse.class);

        assertEquals(videoId, String.valueOf(memoGetMediaResponse.id));
        assertEquals(expectedOriginalFileName, memoGetMediaResponse.fileName);
        assertArrayEquals(expectedContent, memoGetMediaResponse.content);

        // delete
        sendDeleteMediaRequest("/memo/video/", videoId);

        // check doesn't exist
        sendReadMediaRequest("/memo/video/", videoId, status().isNotFound());
    }

    @Test
    public void findAllVideoReturnsAllVideos() throws Exception {
        int expectedResponseSize = 2;

        // create file
        String expectedOriginalFileName = "hello.mp4";
        MockMultipartFile file = new MockMultipartFile("file", expectedOriginalFileName, MediaType.MULTIPART_FORM_DATA_VALUE, "Hello World!".getBytes());
        String videoId = sendCreateMediaRequest("/memo/video", file);

        // create file
        String expectedOtherOriginalFileName = "bye.avi";
        MockMultipartFile otherFile = new MockMultipartFile("file", expectedOtherOriginalFileName, MediaType.MULTIPART_FORM_DATA_VALUE, "Bye World!".getBytes());
        String otherImageId = sendCreateMediaRequest("/memo/video", otherFile);

        String videos = mvc.perform(get("/memo/video"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<MemoFindMediaResponse> mediaResponses = Arrays.asList(mapper.readValue(videos, MemoFindMediaResponse[].class));

        assertEquals(expectedResponseSize, mediaResponses.size());

        assertEquals(videoId, String.valueOf(mediaResponses.get(0).id));
        assertEquals(expectedOriginalFileName, mediaResponses.get(0).name);

        assertEquals(otherImageId, String.valueOf(mediaResponses.get(1).id));
        assertEquals(expectedOtherOriginalFileName, mediaResponses.get(1).name);

        sendDeleteMediaRequest("/memo/video/", videoId);
        sendDeleteMediaRequest("/memo/video/", otherImageId);
    }

    private String sendReadMediaRequest(String urlTemplate, String mediaId, ResultMatcher resultMatcher) throws Exception {
        return mvc.perform(get(urlTemplate + mediaId))
                .andExpect(resultMatcher)
                .andReturn().getResponse().getContentAsString();
    }

    private String sendCreateMediaRequest(String urlTemplate, MockMultipartFile file) throws Exception {
        return mvc.perform(multipart(urlTemplate).file(file))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
    }

    private void sendDeleteMediaRequest(String urlTemplate, String mediaId) throws Exception {
        mvc.perform(delete(urlTemplate + mediaId)).andExpect(status().isOk());
    }
}