package hu.memo.backend.db;

import hu.memo.backend.service.MemoStorageService;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.util.HexFormat;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class MemoStorageServiceTest {

    @Autowired
    MemoStorageService memoStorageService;

    String folderPath = "src/test/resources/media/image/";

    @AfterEach
    public void tearDown() throws IOException {
        FileUtils.deleteDirectory(new File(folderPath));
    }

    @Test
    public void fileSaveWithRandomNameSuccessful() throws IOException {
        int expectedFileCount = getFileCountInFolder(folderPath) + 2;

        byte[] data = HexFormat.of().parseHex("459efa");
        memoStorageService.saveToFileSystem(folderPath, "asdasd.jpeg", data);
        memoStorageService.saveToFileSystem(folderPath, "asdasd.jpeg", data);

        assertEquals(expectedFileCount, getFileCountInFolder(folderPath));
    }

    @Test
    public void readCorrectFileData() throws IOException {
        byte[] expectedData = HexFormat.of().parseHex("459efa");
        String savedFilePath = memoStorageService.saveToFileSystem(folderPath, "asdasd.jpeg", expectedData);

        byte[] actualData = memoStorageService.readFromFileSystem(savedFilePath);
        assertArrayEquals(expectedData, actualData);
    }

    private int getFileCountInFolder(String path) {
        File folder = new File(path);
        File[] files = folder.listFiles();
        return files == null ? 0 : files.length;
    }
}