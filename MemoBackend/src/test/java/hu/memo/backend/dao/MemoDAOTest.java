package hu.memo.backend.dao;

import hu.memo.backend.db.MemoDAO;
import hu.memo.backend.db.entity.Image;
import hu.memo.backend.db.entity.Video;
import hu.memo.backend.db.model.MediaData;
import hu.memo.backend.db.repository.ImageRepository;
import hu.memo.backend.db.repository.VideoRepository;
import hu.memo.backend.exception.MemoException;
import hu.memo.backend.service.MemoStorageService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.HexFormat;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MemoDAOTest {

    @Mock
    ImageRepository imageRepository;

    @Mock
    VideoRepository videoRepository;

    @Mock
    MemoStorageService memoStorageService;

    MemoDAO memoDAO;

    @Captor
    private ArgumentCaptor<String> folderPathCaptor;

    @Captor
    private ArgumentCaptor<String> mediaNameCaptor;

    @Captor
    private ArgumentCaptor<byte[]> contentCaptor;

    @Captor
    private ArgumentCaptor<Image> imageCaptor;

    @Captor
    private ArgumentCaptor<Video> videoCaptor;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        memoDAO = new MemoDAO(imageRepository, videoRepository, memoStorageService);
    }

    @Test
    public void getImageTest() throws IOException, MemoException {
        long imageId = 3L;
        Image image = new Image("test-image", "test/asd.png");
        byte[] expectedData = HexFormat.of().parseHex("a12c4b0f");

        when(imageRepository.findById(eq(imageId))).thenReturn(Optional.of(image));
        when(memoStorageService.readFromFileSystem(image.getPath())).thenReturn(expectedData);

        MediaData<Image> imageMediaData = memoDAO.getImage(imageId);

        assertEquals(image, imageMediaData.getMedia());
        assertArrayEquals(expectedData, imageMediaData.getData());
    }

    @Test
    public void getVideoTest() throws IOException, MemoException {
        // given
        long videoId = 3L;
        Video video = new Video("test-video", "test/asd.mp4");
        byte[] expectedData = HexFormat.of().parseHex("a12c4b0f");

        when(videoRepository.findById(eq(videoId))).thenReturn(Optional.of(video));
        when(memoStorageService.readFromFileSystem(video.getPath())).thenReturn(expectedData);

        // when
        MediaData<Video> videoMediaData = memoDAO.getVideo(videoId);

        // then
        assertEquals(video, videoMediaData.getMedia());
        assertArrayEquals(expectedData, videoMediaData.getData());
    }

    @Test
    public void storeImageTest() throws IOException {
        // given
        String expectedPath = "test/asd.png";
        Image image = new Image("test-image", expectedPath);
        byte[] expectedData = HexFormat.of().parseHex("a12c4b0f");

        when(memoStorageService.saveToFileSystem(folderPathCaptor.capture(), mediaNameCaptor.capture(), contentCaptor.capture())).thenReturn(expectedPath);
        when(imageRepository.save(imageCaptor.capture())).thenReturn(image); // with id

        // when
        Long id = memoDAO.storeImage(image, expectedData);

        // then
        assertEquals("memo/resources/media/image/", folderPathCaptor.getValue());
        assertEquals(image.getName(), mediaNameCaptor.getValue());
        assertArrayEquals(expectedData, contentCaptor.getValue());
        assertEquals(expectedPath, image.getPath());
        assertEquals(image.getId(), id);
    }

    @Test
    public void storeVideoTest() throws IOException {
        // given
        String expectedPath = "test/asd.png";
        Video video = new Video("test-video", expectedPath);
        byte[] expectedData = HexFormat.of().parseHex("a12c4b0f");

        when(memoStorageService.saveToFileSystem(folderPathCaptor.capture(), mediaNameCaptor.capture(), contentCaptor.capture())).thenReturn(expectedPath);
        when(videoRepository.save(videoCaptor.capture())).thenReturn(video); // with id

        // when
        Long id = memoDAO.storeVideo(video, expectedData);

        // then
        assertEquals("memo/resources/media/video/", folderPathCaptor.getValue());
        assertEquals(video.getName(), mediaNameCaptor.getValue());
        assertArrayEquals(expectedData, contentCaptor.getValue());
        assertEquals(expectedPath, video.getPath());
        assertEquals(video.getId(), id);
    }
}