package hu.memo.backend.controller;

import hu.memo.backend.db.entity.Video;
import hu.memo.backend.db.model.MediaData;
import hu.memo.backend.exception.MemoException;
import hu.memo.backend.response.MemoFindMediaResponse;
import hu.memo.backend.response.MemoGetMediaResponse;
import hu.memo.backend.service.MemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("memo/video")
@Slf4j
public class MemoVideoController {

    @Autowired
    MemoService memoService;

    @GetMapping(produces = "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity<List<MemoFindMediaResponse>> findAllVideo() {
        log.debug("Find all videos requested.");
        List<MemoFindMediaResponse> videos = memoService.findAllVideo();
        return new ResponseEntity<>(videos, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity<MemoGetMediaResponse> getVideo(@PathVariable long id) {
        log.debug("Video get requested for id: " + id);
        try {
            MediaData<Video> video = memoService.getVideo(id);
            MemoGetMediaResponse response = new MemoGetMediaResponse(video.getMedia().getId(), video.getMedia().getName(), video.getData());
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (MemoException ex) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Video Not Found", ex);
        }
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public @ResponseBody ResponseEntity<Long> uploadVideo(@RequestParam("file") MultipartFile file) {
        log.debug("File uploaded requested, filename: " + file.getOriginalFilename());
        try {
            Long uploadedVideoId = memoService.uploadVideo(file);
            return new ResponseEntity<>(uploadedVideoId, HttpStatus.CREATED);
        } catch (MemoException ex) {
            throw new ResponseStatusException(HttpStatus.INSUFFICIENT_STORAGE, "Video upload failed", ex);
        }
    }

    @DeleteMapping(value = "/{id}")
    public @ResponseBody ResponseEntity<String> deleteVideo(@PathVariable long id) {
        log.debug("Video delete request for id: " + id);
        try {
            memoService.deleteVideo(id);
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (MemoException ex) {
            log.warn("Video delete failed: " + ex.getMessage(), ex);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Video Not Found", ex);
        }
    }

}
