package hu.memo.backend.controller;

import hu.memo.backend.exception.MemoException;
import hu.memo.backend.response.MemoFindMediaResponse;
import hu.memo.backend.response.MemoGetMediaResponse;
import hu.memo.backend.service.MemoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("memo/image")
@Slf4j
public class MemoImageController {

    @Autowired
    MemoService memoService;

    @GetMapping(produces = "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity<List<MemoFindMediaResponse>> findAllImage() {
        log.debug("Find all images requested.");
        List<MemoFindMediaResponse> images = memoService.findAllImage();
        return new ResponseEntity<>(images, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = "application/json;charset=UTF-8")
    public @ResponseBody ResponseEntity<MemoGetMediaResponse> getImage(@PathVariable long id) {
        log.debug("Image get request for id: " + id);
        try {
            MemoGetMediaResponse image = memoService.getImage(id);
            return new ResponseEntity<>(image, HttpStatus.OK);
        } catch (MemoException ex) {
            log.warn("Image get failed: " + ex.getMessage(), ex);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Image Not Found", ex);
        }
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public @ResponseBody ResponseEntity<Long> uploadImage(@RequestParam(value = "file") MultipartFile file) {
        log.debug("File upload requested, filename: " + file.getOriginalFilename());
        try {
            Long uploadedImageId = memoService.uploadImage(file);
            return new ResponseEntity<>(uploadedImageId, HttpStatus.CREATED);
        } catch (MemoException ex) {
            throw new ResponseStatusException(HttpStatus.INSUFFICIENT_STORAGE, "Image upload failed", ex);
        }
    }

    @DeleteMapping(value = "/{id}")
    public @ResponseBody ResponseEntity<String> deleteImage(@PathVariable long id) {
        log.debug("Image delete request for id: " + id);
        try {
            memoService.deleteImage(id);
            return new ResponseEntity<>("", HttpStatus.OK);
        } catch (MemoException ex) {
            log.warn("Image delete failed: " + ex.getMessage(), ex);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Image Not Found", ex);
        }
    }
}
