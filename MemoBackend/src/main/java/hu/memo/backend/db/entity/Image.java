package hu.memo.backend.db.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "images")
public class Image extends Media {

    public Image() {
        super();
    }
    public Image(String name) {
        super(name);
    }

    public Image(String name, String path) {
        super(name, path);
    }

    @Override
    public String toString() {
        return "Image" + super.toString();
    }
}
