package hu.memo.backend.db.repository;

import hu.memo.backend.db.entity.Video;

public interface VideoRepository extends MediaRepository<Video, Long> {
}
