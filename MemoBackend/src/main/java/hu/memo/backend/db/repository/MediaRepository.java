package hu.memo.backend.db.repository;

import hu.memo.backend.db.entity.Media;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MediaRepository<T extends Media, ID>  extends JpaRepository<T, ID> {
}
