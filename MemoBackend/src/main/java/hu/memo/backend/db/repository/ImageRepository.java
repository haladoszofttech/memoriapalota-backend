package hu.memo.backend.db.repository;

import hu.memo.backend.db.entity.Image;

public interface ImageRepository extends MediaRepository<Image, Long> {
}
