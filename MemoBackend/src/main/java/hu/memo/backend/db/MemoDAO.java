package hu.memo.backend.db;

import hu.memo.backend.service.MemoStorageService;
import hu.memo.backend.db.entity.Image;
import hu.memo.backend.db.entity.Video;
import hu.memo.backend.db.model.MediaData;
import hu.memo.backend.db.repository.ImageRepository;
import hu.memo.backend.db.repository.VideoRepository;
import hu.memo.backend.exception.MemoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Component
public class MemoDAO {

    public static final String MEDIA_RESOURCE_PATH = "memo/resources/media/";

    private final ImageRepository imageRepo;
    private final VideoRepository videoRepo;
    MemoStorageService memoStorageService;

    @Autowired
    public MemoDAO(ImageRepository imageRepository, VideoRepository videoRepository, MemoStorageService memoStorageService) {
        this.imageRepo = imageRepository;
        this.videoRepo = videoRepository;
        this.memoStorageService = memoStorageService;
    }

    public List<Image> findAllImage() {
        return this.imageRepo.findAll();
    }

    public MediaData<Image> getImage(long id) throws MemoException, IOException {
        Optional<Image> imageOptional = this.imageRepo.findById(id);
        if (imageOptional.isPresent()) {
            Image image = imageOptional.get();
            byte[] data = memoStorageService.readFromFileSystem(image.getPath());
            return new MediaData<>(image, data);
        } else {
            throw new MemoException("Couldn't find image: there is no image with this id");
        }
    }

    public Long storeImage(Image image, byte[] fileContent) throws IOException {
        String filePath = memoStorageService.saveToFileSystem(MEDIA_RESOURCE_PATH + "image/", image.getName(), fileContent);
        image.setPath(filePath);
        return imageRepo.save(image).getId();
    }

    public void deleteImage(long id) throws IOException, MemoException {
        Image image = this.getImage(id).getMedia();
        this.imageRepo.delete(image);
        memoStorageService.delete(image.getPath());
    }

    public List<Video> findAllVideo() {
        return this.videoRepo.findAll();
    }

    public MediaData<Video> getVideo(long id) throws MemoException, IOException {
        Optional<Video> videoOptional = this.videoRepo.findById(id);
        if (videoOptional.isPresent()) {
            Video video = videoOptional.get();
            byte[] data = memoStorageService.readFromFileSystem(video.getPath());
            return new MediaData<>(video, data);
        } else {
            throw new MemoException("Couldn't find video: there is no video with this id");
        }
    }

    public Long storeVideo(Video video, byte[] fileContent) throws IOException {
        String filePath = memoStorageService.saveToFileSystem(MEDIA_RESOURCE_PATH + "video/", video.getName(), fileContent);
        video.setPath(filePath);
        return videoRepo.save(video).getId();
    }

    public void deleteVideo(long id) throws IOException, MemoException {
        Video video = this.getVideo(id).getMedia();
        this.videoRepo.delete(video);
        memoStorageService.delete(video.getPath());
    }
}
