package hu.memo.backend.db.model;

import hu.memo.backend.db.entity.Media;

import java.util.Arrays;

public class MediaData<T extends Media> {
    private T media;

    private byte[] data;

    public MediaData(T media, byte[] data) {
        this.media = media;
        this.data = data;
    }

    @Override
    public String toString() {
        return "MediaData{" +
                "media=" + media.toString() +
                ", data=" + Arrays.toString(data) +
                '}';
    }

    public T getMedia() {
        return media;
    }

    public void setMedia(T media) {
        this.media = media;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
