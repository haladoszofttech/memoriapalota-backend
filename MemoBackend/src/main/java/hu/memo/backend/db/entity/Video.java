package hu.memo.backend.db.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "videos")
public class Video extends Media {

    public Video() {
        super();
    }

    public Video(String name) {
        super(name);
    }

    public Video(String name, String path) {
        super(name, path);
    }

    @Override
    public String toString() {
        return "Video" + super.toString();
    }
}
