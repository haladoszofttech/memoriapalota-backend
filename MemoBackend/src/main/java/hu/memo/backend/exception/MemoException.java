package hu.memo.backend.exception;

public class MemoException extends Exception {

    public MemoException(String msg) {
        super(msg);
    }
    public MemoException(String msg, Exception ex) {
        super(msg, ex);
    }
}
