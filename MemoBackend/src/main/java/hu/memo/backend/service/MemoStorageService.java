package hu.memo.backend.service;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class MemoStorageService {

    public String saveToFileSystem(String folderPath, String originalFilePath, byte[] data) throws IOException {
        String fileSuffix = "." + StringUtils.getFilenameExtension(originalFilePath);
        File file = new File(folderPath + "media_" + UUID.randomUUID() + fileSuffix);
        FileUtils.writeByteArrayToFile(file, data);
        return file.getPath();
    }

    public byte[] readFromFileSystem(String filePath) throws IOException {
        File file = new File(filePath);
        return FileUtils.readFileToByteArray(file);
    }

    public void delete(String filePath) throws IOException {
        File file = new File(filePath);
        FileUtils.delete(file);
    }
}