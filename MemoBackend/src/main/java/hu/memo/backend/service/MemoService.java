package hu.memo.backend.service;

import hu.memo.backend.db.MemoDAO;
import hu.memo.backend.db.entity.Image;
import hu.memo.backend.db.entity.Video;
import hu.memo.backend.db.model.MediaData;
import hu.memo.backend.exception.MemoException;
import hu.memo.backend.response.MemoFindMediaResponse;
import hu.memo.backend.response.MemoGetMediaResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component
@Slf4j
public class MemoService {

    MemoDAO memoDAO;

    private static final List<String> allowedImageFormats = List.of("jpg", "jpeg", "png");
    private static final List<String> allowedVideoFormats = List.of("mp4", "mov" , "wmv", "avi");

    @Autowired
    public MemoService(MemoDAO memoDAO) {
        this.memoDAO = memoDAO;
    }

    public List<MemoFindMediaResponse> findAllImage() {
        List<Image> images = memoDAO.findAllImage();
        List<MemoFindMediaResponse> response = new ArrayList<>();
        for (Image image : images) {
            response.add(new MemoFindMediaResponse(image.getId(), image.getName()));
        }
        return response;
    }

    public MemoGetMediaResponse getImage(long id) throws MemoException {
        try {
            MediaData<Image> image = memoDAO.getImage(id);
            return new MemoGetMediaResponse(image.getMedia().getId(), image.getMedia().getName(), image.getData());
        } catch (IOException ex) {
            throw new MemoException("Couldn't find image: " + ex.getMessage(), ex);
        }
    }

    public Long uploadImage(MultipartFile file) throws MemoException {
        log.debug("Service image storing has been started..");
        try {
            String fileName = file.getOriginalFilename();
            if (StringUtils.isBlank(fileName)) {
                fileName = "image.png";
            }
            checkExtensionImage(fileName);
            Image image = new Image(fileName);
            return memoDAO.storeImage(image, file.getBytes());
        } catch (IOException ex) {
            throw new MemoException("Couldn't store image: " + ex.getMessage(), ex);
        }
    }

    public void deleteImage(long id) throws MemoException {
        log.debug("Service image delete has been started..");
        try {
            memoDAO.deleteImage(id);
        } catch (IOException ex) {
            throw new MemoException("Couldn't delete image: " + ex.getMessage(), ex);
        }
    }

    public List<MemoFindMediaResponse> findAllVideo() {
        List<Video> videos = memoDAO.findAllVideo();
        List<MemoFindMediaResponse> response = new ArrayList<>();
        for (Video video : videos) {
            response.add(new MemoFindMediaResponse(video.getId(), video.getName()));
        }
        return response;
    }

    public MediaData<Video> getVideo(long id) throws MemoException {
        log.debug("Service video finding has been started..");
        try {
            return memoDAO.getVideo(id);
        } catch (IOException ex) {
            throw new MemoException("Couldn't find video: " + ex.getMessage(), ex);
        }
    }

    public Long uploadVideo(MultipartFile file) throws MemoException {
        log.debug("Service video storing has been started..");
        try {
            String fileName = file.getOriginalFilename();
            if (StringUtils.isBlank(fileName)) {
                fileName = "video.mp4";
            }
            checkExtensionVideo(fileName);
            Video video = new Video(fileName);
            return memoDAO.storeVideo(video, file.getBytes());
        } catch (IOException ex) {
            throw new MemoException("Couldn't store video: " + ex.getMessage(), ex);
        }
    }

    public void deleteVideo(long id) throws MemoException {
        log.debug("Service video delete has been started..");
        try {
            memoDAO.deleteVideo(id);
        } catch (IOException ex) {
            throw new MemoException("Couldn't delete video: " + ex.getMessage(), ex);
        }
    }

    private void checkExtensionImage(String fileName) throws MemoException {
        String fileExtension = org.springframework.util.StringUtils.getFilenameExtension(fileName);
        if (fileExtension != null && allowedImageFormats.contains(fileExtension.toLowerCase(Locale.ROOT))) {
            return;
        }
        throw new MemoException("Incorrect image format: " + fileExtension);
    }

    private void checkExtensionVideo(String fileName) throws MemoException {
        String fileExtension = org.springframework.util.StringUtils.getFilenameExtension(fileName);
        if (fileExtension != null && allowedVideoFormats.contains(fileExtension.toLowerCase())) {
            return;
        }
        throw new MemoException("Incorrect video format: " + fileExtension);
    }
}