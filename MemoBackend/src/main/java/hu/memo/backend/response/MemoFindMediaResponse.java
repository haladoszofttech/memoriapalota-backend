package hu.memo.backend.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MemoFindMediaResponse {

    @JsonProperty
    public long id;

    @JsonProperty
    public String name;

    public MemoFindMediaResponse() {
    }

    public MemoFindMediaResponse(long id, String name) {
        this.id = id;
        this.name = name;
    }

}
