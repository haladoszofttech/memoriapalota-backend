package hu.memo.backend.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MemoGetMediaResponse {

    @JsonProperty
    public long id;

    @JsonProperty
    public String fileName;

    @JsonProperty
    public byte[] content;

    public MemoGetMediaResponse() {
    }

    public MemoGetMediaResponse(Long id, String name, byte[] data) {
        this.id = id;
        this.fileName = name;
        this.content = data;
    }
}
